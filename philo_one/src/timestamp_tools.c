/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   timestamp_tools.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/21 13:45:22 by jmartel           #+#    #+#             */
/*   Updated: 2021/03/21 13:49:39 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include  "philo_one.h"

long				get_current_relative_timestamp(long start_timestamp)
{
	long		time;
	t_timeval	timeval;

	if (gettimeofday(&timeval, NULL) != 0)
		return -1;
	time = timeval.tv_sec * 1000;
	time += timeval.tv_usec / 1000;
	return (time - start_timestamp);
}

long				get_current_timestamp()
{
	t_timeval	timeval;

	if (gettimeofday(&timeval, NULL) != 0)
		return -1;
	return (timeval.tv_sec * 1000 + timeval.tv_usec / 1000);
}
