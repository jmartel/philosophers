/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philoparser.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 11:05:42 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/13 15:18:10 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philo_two.h>

static t_boolean	check_arguments_validity(char **argv)
{
	int		i;

	i = 1;
	while (argv[i] != NULL)
	{
		if (!ft_isdigit_only(argv[i]))
			return (FALSE);
		i++;
	}
	return (TRUE);
}

t_boolean			check_arguments_range(t_params *params)
{
	if (params->number_of_philosophers < 2)
	{
		return (FALSE);
	}
	if (params->time_to_die <= 0 || params->time_to_eat <= 0 || params->time_to_sleep <= 0)
	{
		return (FALSE);
	}
	return (TRUE);
}

t_boolean			philoparser(t_params *params, int argc, char **argv)
{
	if (argc < 5 || argc > 6)
	{
		dprintf(2, "Invalid arguments number\n");
		return (FALSE);
	}
	if (check_arguments_validity(argv) == FALSE)
	{
		dprintf(2, "Invalid arguments character\n");
		return (FALSE);
	}
	params->number_of_philosophers = ft_atoi(argv[1]);
	params->time_to_die = ft_atoi(argv[2]);
	params->time_to_eat = ft_atoi(argv[3]);
	params->time_to_sleep = ft_atoi(argv[4]);
	if (argv[5] != NULL)
	{
		params->number_of_time_each_philosophers_must_eat = ft_atoi(argv[5]);
		if (params->number_of_time_each_philosophers_must_eat < 0)
		{
			return (FALSE);
		}
	}
	else
	{
		params->number_of_time_each_philosophers_must_eat = -1;
	}
	if (!check_arguments_range(params))
	{
		dprintf(2, "Invalid arguments value\n");
		return (FALSE);
	}
	return (TRUE);
}
