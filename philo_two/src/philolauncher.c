/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philolauncher.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/13 15:24:10 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/13 16:28:44 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_two.h"

static t_boolean	philophore(int n, sem_t **forks, t_running *running)
{
	sem_unlink("forks");
	sem_unlink("running");
	(*forks) = sem_open("forks", O_CREAT, 0777, n);
	if ((*forks) == SEM_FAILED)
	{
		dprintf(2, "Unable to open forks semaphore\n");
		return FALSE;
	}
	running->sem  = sem_open("running", O_CREAT, 0777, 1);
	if (running->sem == SEM_FAILED)
	{
		dprintf(2, "Unable to open running semaphore\n");
		sem_close(*forks);
		return FALSE;
	}
	return TRUE;
}

static t_boolean	philoc(int n, t_philosopher **philosophers)
{
	if ((*philosophers = malloc(sizeof(**philosophers) * n)) == NULL)
		return (FALSE);
	memset(*philosophers, '\0', sizeof(**philosophers) * n);
	return (TRUE);
}

static void			philler(t_params *params, sem_t *forks, t_running *running, long *start_timestamp, t_philosopher *philosphers)
{
	int		i;


	i = 0;
	while (i < params->number_of_philosophers)
	{
		philosphers[i].n = i + 1;
		philosphers[i].params = params;
		philosphers[i].running = running;
		philosphers[i].forks = forks;
		philosphers[i].start_timestamp = start_timestamp;
		i++;
	}
}

static void		philolaunch(int n, t_philosopher *philosophers)
{
	int		i;
	int		ret;

	i = 0;
	while (i < n)
	{
		if ((ret = pthread_create(&(philosophers[i].pthread), NULL, &philosopher, philosophers + i)) != 0)
			dprintf(2, "Unable to launch thread %d: %d\n", i + 1, ret);
		i++;
	}
}

static void		philojoin(int n, t_philosopher *philosophers)
{
	int		i;
	int		ret;
	i = 0;
	while (i < n)
	{
		if ((ret = pthread_detach(philosophers[i].pthread)) != 0)
			dprintf(2, "Unable to join thread: %d (%d)\n", i + 1, ret);
		else
			printf("Joined thread %d\n", i + 1);
		i++;
	}
}

void			philolauncher(t_params *params)
{
	t_running		running;
	sem_t			*forks;
	t_philosopher	*philosophers;
	long			start_timestamp;

	if (philophore(params->number_of_philosophers, &forks, &running) == FALSE)
		return ;
	if (philoc(params->number_of_philosophers, &philosophers) == FALSE)
		return ;
	philler(params, forks, &running, &start_timestamp, philosophers);
	start_timestamp = get_current_timestamp();
	running.running = TRUE;
	philolaunch(params->number_of_philosophers, philosophers);
	while (running.running == TRUE)
		usleep(1000);
	philojoin(params->number_of_philosophers, philosophers);
	sem_close(forks);
	sem_close(running.sem);
	sem_unlink("forks");
	sem_unlink("running");
	free(philosophers);
}
