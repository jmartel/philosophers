#ifndef PHILO_TWO_H
# define PHILO_TWO_H

# include <pthread.h>

/*
** printf
*/
# include <stdio.h>

/*
** malloc
*/
# include <stdlib.h>

/*
** usleep
*/
# include <unistd.h>

/*
** memset
*/
# include <string.h>

/*
** gettimeofday
*/
# include <sys/time.h>

/*
** semaphores
*/
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

#define SLEEP_TIME 100

typedef enum		e_boolean
{
	FALSE = 0,
	TRUE = 1
}					t_boolean;

typedef enum		e_action
{
	FORK,
	EAT,
	SLEEP,
	THINK,
	DEAD
}					t_action;

typedef struct		s_params
{
	int				number_of_philosophers;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				number_of_time_each_philosophers_must_eat;
}					t_params;

typedef struct s_philosopher	t_philosopher;
typedef struct timeval			t_timeval;

typedef struct		s_running
{
	sem_t			*sem;
	t_boolean		running;
}					t_running;


struct				s_philosopher
{
	int				n;
	t_params		*params;
	pthread_t		pthread;
	t_running		*running;
	sem_t			*forks;
	long			*start_timestamp;
	long			last_meal;
};

/*
********************************************************************************
*/

/*
** ft_atoi.c
*/
int					ft_isdigit_only(char *str);
int					ft_atoi(const char *str);

/*
** main.c
*/
int					main(int argc, char **argv);

/*
** philolauncher.c
*/
void				philolauncher(t_params *params);

/*
** philoparser.c
*/
t_boolean			check_arguments_range(t_params *params);
t_boolean			philoparser(
	t_params *params, int argc, char **argv);

/*
** philosopher.c
*/
t_boolean			am_i_alive(t_philosopher *philosopher);
t_boolean			shall_i_continue(t_philosopher *philosopher);
void				show_message(
	t_philosopher *philosopher, t_action action);
t_boolean			get_fork(t_philosopher *philosopher);
t_boolean			eat(t_philosopher *philosopher);
void				*philosopher(void *args);

/*
** timestamp_tools.c
*/
long				get_current_relative_timestamp(long start_timestamp);
long				get_current_timestamp();

#endif
