from reader import Reader


# def main():
from test_coherence import test_coherence

reader = Reader()
reader.parse_content()
if (reader.sorted_lines is None or reader.lines is None):
    print("Error in parsing")
    exit(1)
test_coherence(reader)

# if __name__ == '__main__':
#     main()
