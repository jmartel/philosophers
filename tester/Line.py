from enum import Enum


class Action(Enum):
	FORK = "has taken a fork"
	EAT = "is eating"
	SLEEP = "is sleeping"
	THINK = "is thinking"

class Line:
	SEPARATOR = " "
	COMMENT = "#"

	number = None
	action = None
	timestamp = None

	def __init__(self, line):
		line = str(line)
		if (line.startswith(Line.COMMENT)):
			print("Line: Found a comment")
			return
		split = line.split(Line.SEPARATOR, 2)
		if (len(split) != 3):
			print("Line: Invalid splitting len")
			return
		try:
			self.timestamp = int(split[0])
			self.number = int(split[1])
			split[2] = split[2].strip()
			for action in Action:
				if (action.value == split[2]):
					self.action = action
			if (self.action is None):
				raise Exception("Invalid action: " + split[2])
		except Exception as e:
			print("Error in line formatting: " + line, e)
			self.number = None
			self.timestamp = None
			self.action = None

	def __repr__(self):
		if (self.timestamp is None or self.number is None or self.action is None):
			return "None"
		return str(self.timestamp) + " " + str(self.number) + " " + self.action.value

	def __str__(self):
		return str(self.timestamp) + " " + str(self.number) + " " + self.action.value
