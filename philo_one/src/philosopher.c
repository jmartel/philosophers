/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 14:08:57 by jmartel           #+#    #+#             */
/*   Updated: 2021/04/05 17:49:45 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_one.h"

#include <limits.h>

#define SLEEP_TIME 100

t_boolean		am_i_alive(t_philosopher *philosopher)
{
	long		current_timestamp;

	current_timestamp = get_current_timestamp();
	if (current_timestamp < 0)
	{
		printf("%d: am_i_alive: Unable to get current timestamp\n", philosopher->n);
		return (FALSE);
	}
	if (current_timestamp > philosopher->last_meal + philosopher->params->time_to_die) {
		return (FALSE);
	}
	return (TRUE);
}

t_boolean		shall_i_continue(t_philosopher *philosopher)
{
	if (lockable_bool_read(philosopher->running) == FALSE)
	{
		printf("thread%d: table is not running anymore\n", philosopher->n);
		return FALSE;
	}
	if (am_i_alive(philosopher) == FALSE)
	{
		if (lockable_bool_write_if(philosopher->running, TRUE, FALSE) == TRUE)
			show_message(philosopher, DEAD);
		return (FALSE);
	}
	return (TRUE);
}

t_boolean		lockable_bool_read(t_lockable_bool *lockable)
{
	t_boolean	value;

	pthread_mutex_lock(&lockable->mutex);
	value = lockable->value;
	pthread_mutex_unlock(&lockable->mutex);
	return (value);
}

void			lockable_bool_write(t_lockable_bool *lockable, t_boolean value)
{
	pthread_mutex_lock(&lockable->mutex);
	lockable->value = value;
	pthread_mutex_unlock(&lockable->mutex);
}

t_boolean		lockable_bool_write_if(t_lockable_bool *lockable, t_boolean condition, t_boolean value)
{
	pthread_mutex_lock(&lockable->mutex);
	if (lockable->value == condition)
	{
		lockable->value = value;
		pthread_mutex_unlock(&lockable->mutex);
		return TRUE;
	}
	else
	{
		pthread_mutex_unlock(&lockable->mutex);
		return FALSE;
	}
}

void			show_message(t_philosopher *philosopher, t_action action)
{
	if (action == EAT)
		printf("%ld %d is eating\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else if (action == SLEEP)
		printf("%ld %d is sleeping\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else if (action == THINK)
		printf("%ld %d is thinking\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else if (action == FORK)
		printf("%ld %d has taken a fork\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else if (action == DEAD)
		printf("%ld %d died\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else
		printf("show_message: invalid action received\n");
}

t_boolean		get_fork(t_philosopher *philosopher, t_lockable_bool *fork)
{
	t_boolean		ret;

	while ((ret = lockable_bool_write_if(fork, FALSE, TRUE)) == FALSE)
	{
		usleep(SLEEP_TIME);
		if (shall_i_continue(philosopher) == FALSE)
			return (FALSE);
	}
	if (shall_i_continue(philosopher) == FALSE)
		return (FALSE);
	show_message(philosopher, FORK);
	return (TRUE);
}

t_boolean		eat(t_philosopher *philosopher)
{
	long	end_of_eating;

	philosopher->last_meal = get_current_timestamp();
	end_of_eating = philosopher->last_meal + philosopher->params->time_to_eat;
	if (shall_i_continue(philosopher) == FALSE)
		return (FALSE);
	show_message(philosopher, EAT);
	while (get_current_timestamp() < end_of_eating)
	{
		usleep(SLEEP_TIME);
		if (shall_i_continue(philosopher) == FALSE)
			return (FALSE);
	}
	lockable_bool_write(&philosopher->fork, FALSE);
	lockable_bool_write(&philosopher->neighbour->fork, FALSE);
	return (TRUE);
}

static t_boolean	sleep_time(t_philosopher *philosopher)
{
	long	end_of_sleep_timestamp;

	end_of_sleep_timestamp = get_current_timestamp() + philosopher->params->time_to_sleep;
	if (shall_i_continue(philosopher) == FALSE)
		return (FALSE);
	show_message(philosopher, SLEEP);
	while (get_current_timestamp() < end_of_sleep_timestamp)
	{
		usleep(SLEEP_TIME);
		if (shall_i_continue(philosopher) == FALSE)
			return (FALSE);
	}
	return (TRUE);
}

static t_boolean	think(t_philosopher *philosopher)
{
	if (shall_i_continue(philosopher) == FALSE)
		return (FALSE);
	show_message(philosopher, THINK);
	return (TRUE);
}

void	*philosopher(void *args)
{
	t_philosopher	*philosopher;

	philosopher = (t_philosopher*)args;
	philosopher->last_meal = get_current_timestamp();
	while (philosopher->running->value == TRUE)
	{
		if (get_fork(philosopher, &philosopher->fork) == FALSE) {
			printf("thread%d: break from his fork\n", philosopher->n);
			break ;
		}
		if (get_fork(philosopher, &philosopher->neighbour->fork) == FALSE) {
			printf("thread%d: break from neighbour fork\n", philosopher->n);
			break ;
		}
		if (eat(philosopher) == FALSE) {
			printf("thread%d: break from eat\n", philosopher->n);
			break ;
		}
		if (sleep_time(philosopher) == FALSE) {
			printf("thread%d: break from sleep\n", philosopher->n);
			break ;
		}
		if (think(philosopher) == FALSE) {
			printf("thread%d: break from think\n", philosopher->n);
			break ;
		}
	}
	printf("thread%d: end of thread\n", philosopher->n);
	return NULL;
}
