/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher_launcher.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 15:50:36 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/13 16:04:42 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_one.h"

static t_boolean	philosopher_launcher_allocate(int n,
	t_philosopher **philosophers)
{
	if ((*philosophers = malloc(sizeof(**philosophers) * n)) == NULL)
		return (FALSE);
	memset(*philosophers, '\0', sizeof(**philosophers) * n);
	return (TRUE);
}
 
static void			philosopher_launcher_fill(t_params *params,
	t_lockable_bool *running, t_philosopher *philosophers, long *start_timestamp)
{
	int		i;
	int		ret;

	i = 0;
	while (i < params->number_of_philosophers)
	{
		philosophers[i].n = i + 1;
		philosophers[i].params = params;
		philosophers[i].running = running;
		philosophers[i].start_timestamp = start_timestamp;
		philosophers[i].fork.value = FALSE;
		// TODO protect and free if fail
		if ((ret = pthread_mutex_init(&(philosophers[i].fork.mutex), NULL)) != 0)
			printf("Unable to create fork for: %d\n", i + 1);
		if (i == params->number_of_philosophers - 1)
			philosophers[i].neighbour = philosophers;
		else
			philosophers[i].neighbour = philosophers + i + 1;
		i++;
	}
}

static void			philosopher_launcher_launch(int number_of_philosophers,
	t_philosopher *philosophers, long *start_timestamp)
{
	int			i;
	int			ret;

	i = 0;
	// set start timestamp just before launch
	*start_timestamp = get_current_timestamp();
	while (i < number_of_philosophers)
	{
		if ((ret = pthread_create(&(philosophers[i].pthread), NULL, &philosopher, philosophers + i)) != 0)
			printf("Unable to launch thread %d: %d\n", i + 1, ret);
		i++;
	}
}

static void			philosopher_launcher_join(int number_of_philosophers,
	t_philosopher *philosophers)
{
	int			i;
	int			ret;

	// TODO !!! do not free running ???
	// TODO shall destroy forks after joining ?? 
	i = 0;
	while (i < number_of_philosophers)
	{
		pthread_mutex_destroy(&philosophers[i].fork.mutex);
		i++;
	}
	i = 0;
	while (i < number_of_philosophers)
	{
		if ((ret = pthread_join(philosophers[i].pthread, NULL)) != 0)
			printf("Unable to join thread %d: %d\n", i + 1, ret);
		else
			printf("Joined thread %d\n", i + 1);
		i++;
	}
}

void				philosopher_launcher(t_params *params)
{
	t_boolean			ret;
	t_philosopher		*philosophers;
	t_lockable_bool		running;
	long				start_timestamp;

	ret = philosopher_launcher_allocate(params->number_of_philosophers, &philosophers);
	if (ret == FALSE)
		return ;
	running.value = TRUE;
	pthread_mutex_init(&(running.mutex), NULL);
	philosopher_launcher_fill(params, &running, philosophers, &start_timestamp);
	philosopher_launcher_launch(params->number_of_philosophers, philosophers, &start_timestamp);
	while (running.value == TRUE)
		usleep(1000);
	philosopher_launcher_join(params->number_of_philosophers, philosophers);
	free(philosophers);
}
