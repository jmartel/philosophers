/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_one.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/08 15:42:17 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/13 15:21:10 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_ONE_H
# define PHILO_ONE_H

# include <pthread.h>

/*
** printf
*/
# include <stdio.h>

/*
** malloc
*/
# include <stdlib.h>

/*
** usleep
*/
# include <unistd.h>

/*
** memset
*/
# include <string.h>

/*
** gettimeofday
*/
# include <sys/time.h>

typedef enum		e_boolean
{
	FALSE = 0,
	TRUE = 1
}					t_boolean;

typedef enum		e_action
{
	FORK,
	EAT,
	SLEEP,
	THINK,
	DEAD
}					t_action;

typedef struct		s_params
{
	int				number_of_philosophers;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				number_of_time_each_philosophers_must_eat;
}					t_params;

typedef struct		s_lockable_bool
{
	pthread_mutex_t	mutex;
	t_boolean		value;
}					t_lockable_bool;

typedef struct s_philosopher	t_philosopher;
typedef struct timeval			t_timeval;

struct				s_philosopher
{
	int				n;
	t_params		*params;
	pthread_t		pthread;
	t_lockable_bool	*running;
	t_lockable_bool	fork;
	t_philosopher	*neighbour;
	long			*start_timestamp;
	long			last_meal;
};

/*
********************************************************************************
*/

/*
** ft_atoi.c
*/
int					ft_isdigit_only(char *str);
int					ft_atoi(const char *str);

/*
** main.c
*/
int					main(int argc, char **argv);

/*
** philoparser.c
*/
t_boolean			philoparser(
	t_params *params, int argc, char **argv);

/*
** philosopher.c
*/
t_boolean			am_i_alive(t_philosopher *philosopher);
t_boolean			shall_i_continue(t_philosopher *philosopher);
t_boolean			lockable_bool_read(t_lockable_bool *lockable);
void				lockable_bool_write(
	t_lockable_bool *lockable, t_boolean value);
t_boolean			lockable_bool_write_if(
	t_lockable_bool *lockable, t_boolean condition, t_boolean value);
void				show_message(
	t_philosopher *philosopher, t_action action);
t_boolean			get_fork(
	t_philosopher *philosopher, t_lockable_bool *fork);
t_boolean			eat(t_philosopher *philosopher);
void				*philosopher(void *args);

/*
** philosopher_launcher.c
*/
void				philosopher_launcher(t_params *params);

/*
** timestamp_tools.c
*/
long				get_current_relative_timestamp(long start_timestamp);
long				get_current_timestamp();

#endif
