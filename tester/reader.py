import sys
import os

from Line import Line


class Reader:
	fd = None

	def __init__(self):
		self.sorted_lines = dict()
		self.lines = []

		if (len(sys.argv) > 1):
			if (os.path.isfile(sys.argv[1])):
				self.fd = open(sys.argv[1], 'r')
			else :
				return
		else :
			self.fd = sys.stdin

	def parse_content(self):
		if (self.fd is None):
			return
		rawLines = self.fd.readlines()
		self.lines = [len(rawLines)]
		for i in range(0, len(rawLines) - 1):
			line = Line(rawLines[i])
			# self.lines[i] = line
			if (not self.sorted_lines.get(line.number)):
				self.sorted_lines[line.number] = []
			self.sorted_lines[line.number].append(line)
