from Line import Action
from reader import Reader

def	show_context(list, i):
	minimum = max(0, i - 5)
	maximum = min(len(list), i + 6)
	print("-----------------------------------------------------------------------")
	for index in range(minimum, maximum):
		if (index == i):
			print(str(list[index]) + " <-- ERROR")
		else:
			print(list[index])
	print("-----------------------------------------------------------------------")

def test_coherence(reader):
	for key in reader.sorted_lines.keys():
		list = reader.sorted_lines.get(key)
		for i in range(0, len(list)):
			line = list[i]
			if (i % 5 == 0 and list[i].action != Action.FORK):
				print("i: %d | i % 5: %d" % (i, i % 5))
				print("coherence: invalid action (%d: %d): %s (shall be %s)" % (line.number, line.timestamp, line.action.name, Action.FORK.name))
				show_context(list, i)
			elif (i % 5 == 1 and line.action != Action.FORK):
				print("i: %d | i % 5: %d" % (i, i % 5))
				print("coherence: invalid action (%d: %d): %s (shall be %s) (second)" % (line.number, line.timestamp, line.action.name, Action.FORK.name))
				show_context(list, i)
			elif (i % 5 == 2 and line.action != Action.EAT):
				print("i: %d | i % 5: %d" % (i, i % 5))
				print("coherence: invalid action (%d: %d): %s (shall be %s)" % (line.number, line.timestamp, line.action.name, Action.EAT.name))
				show_context(list, i)
			elif (i % 5 == 3 and line.action != Action.SLEEP):
				print("i: %d | i % 5: %d" % (i, i % 5))
				print("coherence: invalid action (%d: %d): %s (shall be %s)" % (line.number, line.timestamp, line.action.name, Action.SLEEP.name))
				show_context(list, i)
			elif (i % 5 == 4 and line.action != Action.THINK):
				print("i: %d | i % 5: %d" % (i, i % 5))
				print("coherence: invalid action (%d: %d): %s (shall be %s)" % (line.number, line.timestamp, line.action.name, Action.THINK.name))
				show_context(list, i)
	print("Coherence: END OF TEST")
