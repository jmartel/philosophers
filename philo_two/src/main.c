/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/13 15:23:39 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/13 16:13:32 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_two.h"

static void		show_usage(void)
{
	printf("Usage: ./philo_one number_of_philosopher time_to_die time_to_eat ");
	printf("time_to_sleep [number_of_time_each_philosophers_must_eat]\n");
}

static void		show_arguments(t_params *params)
{
	printf("number_of_philosopher: %d\n", params->number_of_philosophers);
	printf("time_to_die: %d\n", params->time_to_die);
	printf("time_to_eat: %d\n", params->time_to_eat);
	printf("time_to_sleep: %d\n", params->time_to_sleep);
	printf("number_of_time_each_philosophers_must_eat: %d\n",
		params->number_of_time_each_philosophers_must_eat);
}


int				main(int argc, char **argv)
{
	t_params		params;

	if (philoparser(&params, argc, argv) == FALSE)
	{
		show_usage();
		return (1);
	}
	show_arguments(&params);
	philolauncher(&params);
	return (0);
}
