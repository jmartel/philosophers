/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosopher.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmartel <jmartel@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/13 16:10:28 by jmartel           #+#    #+#             */
/*   Updated: 2021/05/13 16:18:56 by jmartel          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_two.h"

t_boolean	am_i_alive(t_philosopher *philosopher)
{
	long		current_timestamp;

	current_timestamp = get_current_timestamp();
	if (current_timestamp < 0)
	{
		printf("%d: am_i_alive: Unable to get current timestamp\n", philosopher->n);
		return (FALSE);
	}
	if (current_timestamp > philosopher->last_meal + philosopher->params->time_to_die) {
		return (FALSE);
	}
	return (TRUE);
}

t_boolean		shall_i_continue(t_philosopher *philosopher)
{
	// TODO ret value
	sem_wait(philosopher->running->sem);
	if (philosopher->running->running == FALSE)
	{
		printf("thread%d: table is not running anymore\n", philosopher->n);
		sem_post(philosopher->running->sem);
		return FALSE;
	}
	if (am_i_alive(philosopher) == FALSE)
	{
		if (philosopher->running->running == TRUE)
			show_message(philosopher, DEAD);
		philosopher->running->running = FALSE;
		sem_post(philosopher->running->sem);
		return (FALSE);
	}
	sem_post(philosopher->running->sem);
	return (TRUE);
}

void			show_message(t_philosopher *philosopher, t_action action)
{
	if (action == EAT)
		printf("%ld %d is eating\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else if (action == SLEEP)
		printf("%ld %d is sleeping\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else if (action == THINK)
		printf("%ld %d is thinking\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else if (action == FORK)
		printf("%ld %d has taken a fork\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else if (action == DEAD)
		printf("%ld %d died\n", get_current_relative_timestamp(*philosopher->start_timestamp), philosopher->n);
	else
		printf("show_message: invalid action received\n");
}

t_boolean		get_fork(t_philosopher *philosopher)
{
	sem_wait(philosopher->forks);
	if (shall_i_continue(philosopher) == FALSE)
		return (FALSE);
	show_message(philosopher, FORK);
	return (TRUE);
}

t_boolean		eat(t_philosopher *philosopher)
{
	long	end_of_eating;

	philosopher->last_meal = get_current_timestamp();
	end_of_eating = philosopher->last_meal + philosopher->params->time_to_eat;
	if (shall_i_continue(philosopher) == FALSE)
		return (FALSE);
	show_message(philosopher, EAT);
	while (get_current_timestamp() < end_of_eating)
	{
		usleep(SLEEP_TIME);
		if (shall_i_continue(philosopher) == FALSE)
			return (FALSE);
	}
	sem_post(philosopher->forks);
	sem_post(philosopher->forks);
	return (TRUE);
}

static t_boolean	sleep_time(t_philosopher *philosopher)
{
	long	end_of_sleep_timestamp;

	end_of_sleep_timestamp = get_current_timestamp() + philosopher->params->time_to_sleep;
	if (shall_i_continue(philosopher) == FALSE)
		return (FALSE);
	show_message(philosopher, SLEEP);
	while (get_current_timestamp() < end_of_sleep_timestamp)
	{
		usleep(SLEEP_TIME);
		if (shall_i_continue(philosopher) == FALSE)
			return (FALSE);
	}
	return (TRUE);
}

static t_boolean	think(t_philosopher *philosopher)
{
	if (shall_i_continue(philosopher) == FALSE)
		return (FALSE);
	show_message(philosopher, THINK);
	return (TRUE);
}

void	*philosopher(void *args)
{
	t_philosopher	*philosopher;

	philosopher = (t_philosopher*)args;
	philosopher->last_meal = get_current_timestamp();
	while (philosopher->running->running == TRUE)
	{
		if (get_fork(philosopher) == FALSE) {
			printf("thread%d: break from his fork\n", philosopher->n);
			break ;
		}
		if (get_fork(philosopher) == FALSE) {
			printf("thread%d: break from neighbour fork\n", philosopher->n);
			break ;
		}
		if (eat(philosopher) == FALSE) {
			printf("thread%d: break from eat\n", philosopher->n);
			break ;
		}
		if (sleep_time(philosopher) == FALSE) {
			printf("thread%d: break from sleep\n", philosopher->n);
			break ;
		}
		if (think(philosopher) == FALSE) {
			printf("thread%d: break from think\n", philosopher->n);
			break ;
		}
	}
	printf("thread%d: end of thread\n", philosopher->n);
	return NULL;
}
